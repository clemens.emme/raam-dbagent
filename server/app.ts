import connector from 'mongo-backoff-connector';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as express from 'express';
import * as log from 'fancy-log';
import * as helmet from 'helmet';
import * as http from 'http-error-handler';
import * as mongoose from 'mongoose';
import * as morgan from 'morgan';

import setRoutes from './routes';

const app = express();

/**
 * Connect to database
 */
(mongoose as any).Promise = global.Promise;
connector({
    name: 'RaaM',
    // host: 'localhost',
    // port: '32768',
    host: 'database',
    port: '27017',
})
    .then(() => {
        config();
        startServer();
    })
    .catch((err) => {
        log.error(err);
        process.exit();
    });

/**
 * Configure server
 */
function config() {

    // middleware
    app.use(morgan('dev'));
    app.use(helmet());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(cors());
    app.options('*', cors());

    setRoutes(app);

    // error handler
    app.use(http.errorHandler);
    app.use(http.notFoundHandler);
}

/**
 * Start the server
 */
function startServer() {
    app.listen('3000', () => {
        log.info(`Service running in ${app.get('env')} mode\n`);
    });
}

module.exports = app;
