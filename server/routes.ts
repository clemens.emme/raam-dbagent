import * as express from 'express';
import PersonCtrl from './controllers/personCtrl';
import RoleCtrl from './controllers/roleCtrl';
import WorkLogCtrl from './controllers/workLogCtrl';
import ContainerCtrl from './controllers/containerCtrl';
import TaskCtrl from './controllers/taskCtrl';
import RepositoryCtrl from './controllers/repositoryCtrl';
import TaskTypeCtrl from './controllers/taskTypeCtrl';

export default function setRoutes(app) {

    const router = express.Router();

    const roleCtrl = new RoleCtrl();
    const taskCtrl = new TaskCtrl();
    const personCtrl = new PersonCtrl();
    const workLogCtrl = new WorkLogCtrl();
    const taskTypeCtrl = new TaskTypeCtrl();
    const containerCtrl = new ContainerCtrl();
    const repositoryCtrl = new RepositoryCtrl();

    // id addition
    const idAdditionApiRoute = '/:id';

    // workLog route
    const workLogApiRoute = '/workLog';
    const workLogApiIdRoute = workLogApiRoute + idAdditionApiRoute;

    // taskType route
    const taskTypeApiRoute = '/taskType';
    const taskTypeApiIdRoute = taskTypeApiRoute + idAdditionApiRoute;

    // task route
    const taskApiRoute = '/task';
    const taskApiIdRoute = taskApiRoute + idAdditionApiRoute;

    // role route
    const roleApiRoute = '/role';
    const roleApiIdRoute = roleApiRoute + idAdditionApiRoute;

    // container route
    const containerApiRoute = '/container';
    const containerApiIdRoute = containerApiRoute + idAdditionApiRoute;

    // repository route
    const repositoryApiRoute = '/repository';
    const repositoryApiIdRoute = repositoryApiRoute + idAdditionApiRoute;

    // person route
    const personApiRoute = '/person';
    const personApiIdRoute = personApiRoute + idAdditionApiRoute;
    const loginAdditionApiRoute = '/login';
    const personApiLoginRoute = personApiRoute + loginAdditionApiRoute;

    // =====================================================================================
    //                                    persons
    // =====================================================================================

    router.route(personApiRoute).get(personCtrl.getAll);
    router.route(personApiRoute).post(personCtrl.insert);

    router.route(personApiLoginRoute).post(personCtrl.login);

    router.route(personApiIdRoute).get(personCtrl.get);
    router.route(personApiIdRoute).put(personCtrl.update);
    router.route(personApiIdRoute).delete(personCtrl.delete);

    // =====================================================================================
    //                                    role
    // =====================================================================================

    router.route(roleApiRoute).get(roleCtrl.getAll);
    router.route(roleApiRoute).post(roleCtrl.insert);

    router.route(roleApiIdRoute).get(roleCtrl.get);
    router.route(roleApiIdRoute).put(roleCtrl.update);
    router.route(roleApiIdRoute).delete(roleCtrl.delete);

    // =====================================================================================
    //                                    workLog
    // =====================================================================================

    router.route(workLogApiRoute).get(workLogCtrl.getAll);
    router.route(workLogApiRoute).post(workLogCtrl.insert);

    router.route(workLogApiIdRoute).get(workLogCtrl.get);
    router.route(workLogApiIdRoute).put(workLogCtrl.update);
    router.route(workLogApiIdRoute).delete(workLogCtrl.delete);

    // =====================================================================================
    //                                    taskType
    // =====================================================================================

    router.route(taskTypeApiRoute).get(taskTypeCtrl.getAll);
    router.route(taskTypeApiRoute).post(taskTypeCtrl.insert);

    router.route(taskTypeApiIdRoute).get(taskTypeCtrl.get);
    router.route(taskTypeApiIdRoute).put(taskTypeCtrl.update);
    router.route(taskTypeApiIdRoute).delete(taskTypeCtrl.delete);

    // =====================================================================================
    //                                    container
    // =====================================================================================

    router.route(containerApiRoute).get(containerCtrl.getAll);
    router.route(containerApiRoute).post(containerCtrl.insert);

    router.route(containerApiIdRoute).get(containerCtrl.get);
    router.route(containerApiIdRoute).put(containerCtrl.update);
    router.route(containerApiIdRoute).delete(containerCtrl.delete);

    // =====================================================================================
    //                                    task
    // =====================================================================================

    router.route(taskApiRoute).get(taskCtrl.getAll);
    router.route(taskApiRoute).post(taskCtrl.insert);

    router.route(taskApiIdRoute).get(taskCtrl.get);
    router.route(taskApiIdRoute).put(taskCtrl.update);
    router.route(taskApiIdRoute).delete(taskCtrl.delete);

    // =====================================================================================
    //                                    repository
    // =====================================================================================

    router.route(repositoryApiRoute).get(repositoryCtrl.getAll);
    router.route(repositoryApiRoute).post(repositoryCtrl.insert);

    router.route(repositoryApiIdRoute).get(repositoryCtrl.get);
    router.route(repositoryApiIdRoute).put(repositoryCtrl.update);
    router.route(repositoryApiIdRoute).delete(repositoryCtrl.delete);

    app.use('/api', router);
}
