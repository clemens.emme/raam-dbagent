abstract class BaseCtrl {

    public abstract model: any;

    // Get all
    public getAll = (req, res) => {
        this.model.find({}, (err, docs) => {
            if (err) {
                return console.error(err);
            }
            res.json(docs);
        });
    };

    // Insert
    public insert = (req, res) => {
        const obj = new this.model(req.body);
        obj.save((err, item) => {
            // 11000 is the code for duplicate key error
            if (err && err.code === 11000) {
                res.sendStatus(400);
            }
            if (err) {
                return console.error(err);
            }
            res.status(200).json(item);
        });
    };

    // Get by id
    public get = (req, res) => {
        this.model.findOne({_id: req.params.id}, (err, obj) => {
            if (err) {
                return console.error(err);
            }
            res.json(obj);
        });
    };

    // Update by id
    public update = (req, res) => {
        this.model.findOneAndUpdate({_id: req.params.id}, req.body, {
            new: true,
            upsert: false,
            runValidators: true,
        }, (err, obj) => {
            if (err) {
                return console.error(err);
            }
            res.json(obj);
        });

    };

    // Delete by id
    public delete = (req, res) => {
        this.model.findOneAndRemove({_id: req.params.id}, (obj) => {
            res.status(200).json(obj);
        }, (err) => {
            if (err) {
                return console.error(err);
            }
            res.sendStatus(200);
        });
    };
}

export default BaseCtrl;
