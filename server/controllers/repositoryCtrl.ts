import BaseCtrl from './base';
import RepositoryModel from '../models/repositoryModel';
import * as mongoose from 'mongoose';

export default class RepositoryCtrl extends BaseCtrl {

    public model = RepositoryModel;

    // overwrite get
    // get repo by id or name
    public get = (req, res) => {
        this.model.findOne(this.getCondition(req.params.id), (err, obj) => {
            if (err) {
                return console.error(err);
            }
            res.json(obj);
        });
    };

    // Decide between id or name in params
    private getCondition(idOrName) {
        if (/\d/.test(idOrName)) {
            console.log('is id');
            return {_id: idOrName};
        } else {
            console.log('is name');
            return {name: idOrName};
        }
    }
}
