import BaseCtrl from './base';
import TaskModel from '../models/taskModel';

export default class TaskCtrl extends BaseCtrl {

    public model = TaskModel;
}
