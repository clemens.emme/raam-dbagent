import BaseCtrl from './base';
import TaskTypeModel from '../models/taskTypeModel';

export default class TaskTypeCtrl extends BaseCtrl {

    public model = TaskTypeModel;
}
