import BaseCtrl from './base';
import RoleModel from '../models/roleModel';

export default class RoleCtrl extends BaseCtrl {

    public model = RoleModel;
}
