import BaseCtrl from './base';
import ContainerModel from '../models/containerModel';

export default class ContainerCtrl extends BaseCtrl {

    public model = ContainerModel;
}
