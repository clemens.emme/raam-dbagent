import BaseCtrl from './base';
import PersonModel from '../models/personModel';

export default class PersonCtrl extends BaseCtrl {

    public model = PersonModel;

    // get One by username and password
    public login = (req, res) => {
        this.model.findOne({username: req.body.username, password: req.body.password}, (err, docs) => {
            if (err) {
                return console.error(err);
            }
            res.json(docs);
        });
    };
}
