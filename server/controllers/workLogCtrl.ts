import BaseCtrl from './base';
import WorkLogModel from '../models/workLogModel';

export default class WorkLogCtrl extends BaseCtrl {

    public model = WorkLogModel;
}
