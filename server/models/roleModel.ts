import * as mongoose from 'mongoose';

const roleDataSchema = new mongoose.Schema({

    name: {type: String, trim: true, required: true},
    color: {type: String, trim: true, default: '#FF00FF'}, // Fuchsia

    // ================================================================================================================
    // Background Data
    // ================================================================================================================

    last_modified_date: {type: Date, default: Date.now},
    created_date: {type: Date, default: Date.now},
});

roleDataSchema
    .pre('save', function(this, next) {
        // Generating a new timestamp for the asset each time a save is made
        this.last_modified_date = Date.now();
        next();
    });

const RoleModel = mongoose.model('roles', roleDataSchema);

export default RoleModel;
