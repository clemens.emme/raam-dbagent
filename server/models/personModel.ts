import * as mongoose from 'mongoose';

const defaultPersonalWords = 'This is basic user profile with image, username, detail and button.';

const personDataSchema = new mongoose.Schema({

    username: {type: String, trim: true, required: true},
    email: {type: String, trim: true, default: ''},
    password: {type: String, trim: true, default: ''},

    firstName: {type: String, trim: true, default: ''},
    lastName: {type: String, trim: true, default: ''},
    personalWords: {type: String, trim: true, default: defaultPersonalWords},

    roles: [{type: mongoose.Schema.Types.ObjectId, ref: 'roles', default: []}],
    credentials: [{
        repository: {type: mongoose.Schema.Types.ObjectId, ref: 'repositories', default: undefined},
        username: {type: String, trim: true, default: ''},
        password: {type: String, trim: true, default: ''},
    }],

    // ================================================================================================================
    // Background Data
    // ================================================================================================================

    last_modified_date: {type: Date, default: Date.now},
    created_date: {type: Date, default: Date.now},
});

// Auto populate the necessary fields
const listAutoPopulate = function(this, next) {
    this.populate('roles');
    next();
};

// Auto populate the necessary fields
const autoPopulate = function(this, next) {
    this.populate('roles');
    this.populate('credentials.repository');
    next();
};

personDataSchema
    .pre('find', listAutoPopulate)
    .pre('findOne', autoPopulate)
    .pre('save', function(this, next) {
        // Generating a new timestamp for the asset each time a save is made
        this.last_modified_date = Date.now();
        next();
    });

const PersonModel = mongoose.model('persons', personDataSchema);

export default PersonModel;
