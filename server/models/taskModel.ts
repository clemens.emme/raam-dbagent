import * as mongoose from 'mongoose';

const TaskDataSchema = new mongoose.Schema({

    title: {type: String, trim: true, required: true},
    branchName: {type: String, trim: true, default: ''},
    filePath: {type: String, trim: true, default: ''},
    description: {type: String, trim: true, default: ''},
    estimatedPoints: {type: Number, default: 0},

    type: {type: mongoose.Schema.Types.ObjectId, ref: 'taskTypes', required: true},
    container: {type: mongoose.Schema.Types.ObjectId, ref: 'containers', default: undefined},
    assignee: {type: mongoose.Schema.Types.ObjectId, ref: 'persons', default: undefined},
    parent: {type: mongoose.Schema.Types.ObjectId, ref: 'tasks', default: undefined},
    children: [{type: mongoose.Schema.Types.ObjectId, ref: 'tasks', default: []}],
    workLogs: [{type: mongoose.Schema.Types.ObjectId, ref: 'workLogs', default: []}],

    // ================================================================================================================
    // Background Data
    // ================================================================================================================

    last_modified_date: {type: Date, default: Date.now},
    created_date: {type: Date, default: Date.now},
});

// ================================================================================================================
// 1-n
// n-n Not In -> In | In -> Not In
// 1-1 Empty -> Set  |  Set -> Empty  | Set -> new Set
// ================================================================================================================

// TODO Validate if all least one parameter has the name 'name'
// TODO this is because the generated file needs to get at least a name

// Auto populate the necessary fields
const listAutoPopulate = function(this, next) {
    this.populate('type');
    this.populate('assignee', ['firstName', 'lastName', 'username']);
    next();
};

// Auto populate the necessary fields
const autoPopulate = function(this, next) {
    this.populate('type');
    this.populate('container');
    this.populate('assignee');
    this.populate('parent');
    this.populate('children');
    this.populate('workLogs');
    next();
};

// TODO Validate if DB objects are parsed to ids yet

// ================================================================================================================
// Finishing
// ================================================================================================================

TaskDataSchema
    .pre('find', listAutoPopulate)
    .pre('findOne', autoPopulate)
    .pre('save', function(this, next) {
        // Generating a new timestamp for the asset each time a save is made
        this.last_modified_date = Date.now();
        next();
    });

const TaskModel = mongoose.model('tasks', TaskDataSchema);

export default TaskModel;
