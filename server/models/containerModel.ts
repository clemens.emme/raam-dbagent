import * as mongoose from 'mongoose';

const ContainerDataSchema = new mongoose.Schema({

    url: {type: String, trim: true, required: true},
    name: {type: String, trim: true, required: true},
    format: {type: String, trim: true, required: true},
    color: {type: String, trim: true, default: '#FF00FF'}, // Fuchsia
    description: {type: String, trim: true, default: ''},
    parameters: [{
        name: {type: String, trim: true, required: true},
        type: {type: String, trim: true, required: true},
        example: {type: String, trim: true, default: '...'},
    }],

    // ================================================================================================================
    // Background Data
    // ================================================================================================================

    last_modified_date: {type: Date, default: Date.now},
    created_date: {type: Date, default: Date.now},
});

// TODO Validate if all least one parameter has the name 'name'
// TODO this is because the generated file needs to get at least a name

ContainerDataSchema
    .pre('save', function(this, next) {
        // Generating a new timestamp for the asset each time a save is made
        this.last_modified_date = Date.now();
        next();
    });

const ContainerModel = mongoose.model('containers', ContainerDataSchema);

export default ContainerModel;
