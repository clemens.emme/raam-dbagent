import * as mongoose from 'mongoose';

const WorkLogDataSchema = new mongoose.Schema({

    hoursSpend: {type: Number, default: 0},
    minutesSpend: {type: Number, default: 0},
    commitTitle: {type: String, trim: true, default: ''},
    commitDescription: {type: String, trim: true, default: ''},

    // ================================================================================================================
    // Background Data
    // ================================================================================================================

    last_modified_date: {type: Date, default: Date.now},
    created_date: {type: Date, default: Date.now},
});

WorkLogDataSchema
    .pre('save', function(this, next) {
        // Generating a new timestamp for the asset each time a save is made
        this.last_modified_date = Date.now();
        next();
    });

const WorkLogModel = mongoose.model('workLogs', WorkLogDataSchema);

export default WorkLogModel;
