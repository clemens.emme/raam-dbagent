import * as mongoose from 'mongoose';

const repositoryDataSchema = new mongoose.Schema({

    url: {type: String, trim: true, required: true},
    name: {type: String, trim: true, required: true},

    tasks: [{type: mongoose.Schema.Types.ObjectId, ref: 'tasks', default: []}],

    // ================================================================================================================
    // Background Data
    // ================================================================================================================

    last_modified_date: {type: Date, default: Date.now},
    created_date: {type: Date, default: Date.now},
});

// Auto populate the necessary fields
const listAutoPopulate = function(this, next) {
    this.populate('tasks', 'title');
    next();
};

const autoPopulate = function(this, next) {
    this.populate('tasks');
    next();
};

repositoryDataSchema
    .pre('find', listAutoPopulate)
    .pre('findOne', autoPopulate)
    .pre('save', function(this, next) {
        // Generating a new timestamp for the asset each time a save is made
        this.last_modified_date = Date.now();
        next();
    });

const RepositoryModel = mongoose.model('repositories', repositoryDataSchema);

export default RepositoryModel;
